﻿using AkmetronTask.Data;
using AkmetronTask.IO;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AkmetronTask
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Model model;

        public MainWindow()
        {
            var loaderSaver = new JSONFileLoaderSaverForProceduresOfSignals();
            model = new Model(loaderSaver, loaderSaver);
            InitializeComponent();
            DataContext = model;
            model.PropertyChanged += Model_PropertyChanged;
            gbSpecificSignalInfo.Content = model.CurrentSignal.Data;
        }
        

        #region EventHandlers

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(model.CurrentSignal):
                    if (model.CurrentSignal == null)
                        return;
                    Dispatcher.Invoke(() => gbSpecificSignalInfo.Content = model.CurrentSignal.Data);
                    break;
            }
        }



        private int curIndex;

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var lb = sender as ListBox;
            var newSelectedItem = e.AddedItems?.Count > 0 ? e.AddedItems[0] : null;
            if (newSelectedItem != null)
            {
                (lb).ScrollIntoView(newSelectedItem);
                curIndex = lb.SelectedIndex;
            }
            else if (lb.Items.Count > 0)
            {
                if (curIndex >= lb.Items.Count)
                    curIndex = lb.Items.Count - 1;
                lb.SelectedItem = lb.Items[curIndex];
            }
        }

        private void ListBox_GotFocus(object sender, RoutedEventArgs e)
        {
            listBoxSignals.SelectedItem = listBoxSignals.SelectedItem;
        }
        private void BtnNewSignalClick(object sender, RoutedEventArgs e)
        {
            model.UpdateCurrentSignal();
        }

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
            model.AddCurrentSignalToProcedure();
        }

        private void BtnRemoveClick(object sender, RoutedEventArgs e)
        {
            model.RemoveCurrentSignalFromProcedure();
        }

        #endregion

        #region Commands EventHandlers

        private void Play_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = model.Elements.Count > 0 && model.State != ProcedureState.WORKING;
        }

        private void Play_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            model.Start();
        }

        private void Pause_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = model.Elements.Count > 0 && model.State == ProcedureState.WORKING;
        }

        private void Pause_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            model.Pause();
        }

        private void Stop_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = model.Elements.Count > 0 && model.State != ProcedureState.STOPPED;
        }

        private void Stop_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            model.Stop();
        }

        private void New_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = model.State == ProcedureState.STOPPED;
        }

        private void New_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            model.New();
        }

        private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = model.State == ProcedureState.STOPPED;
        }

        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            model.Open();
        }

        private void Save_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = model.Elements.Count > 0 && model.State == ProcedureState.STOPPED;
        }

        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            model.Save();
        }

        #endregion

    }
}
