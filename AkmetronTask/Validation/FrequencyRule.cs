﻿using System.Globalization;
using System.Linq;
using System.Windows.Controls;

namespace AkmetronTask.Validation
{
    public class FrequencyRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string s = ((string)value).Trim();

            if (string.IsNullOrWhiteSpace(s))
                return new ValidationResult(false, "Сторка не должна быть пустой");

            string[] split = s.Split().Select(w => w.Trim().ToLower()).Where(w => !string.IsNullOrEmpty(w)).ToArray();

            if (split.Length != 2)
                return new ValidationResult(false, "Сторка должна состоять из двух частей, разделенных проблеом - числа и единицы измерения");

            if (!double.TryParse(split[0], out double freq))
                return new ValidationResult(false, "Ошибка в числе: недопустимые символы или слишком большое для типа Double число");

            string[] measures = new string[] { "гц", "кгц", "мгц", "ггц" };

            if (!measures.Contains(split[1]))
                return new ValidationResult(false, "Ошибка в единице измерения: принимаются только Гц, КГц, МГц и ГГц (регистр букв не имеет значения)");


            if (freq < 0 || freq > 500)
                return new ValidationResult(false, "Ошибка в числе: допускаются только значения от 0 до 500 (включительно)");

            return new ValidationResult(true, null);
        }
    }
}
