﻿using System.Globalization;
using System.Windows.Controls;

namespace AkmetronTask.Validation
{
    public class PhaseShiftRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string s = ((string)value).Trim();

            if (string.IsNullOrWhiteSpace(s))
                return new ValidationResult(false, "Сторка не должна быть пустой");

            if (!double.TryParse(s, out double ps))
                return new ValidationResult(false, "Ошибка в числе: недопустимые символы или слишком большое для типа Double число");


            if (ps < 0 || ps >= 360)
                return new ValidationResult(false, "Ошибка в числе: допускаются только значения от 0 до 360 (исключая 360)");

            return new ValidationResult(true, null);
        }
    }
}
