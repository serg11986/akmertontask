﻿using System.Globalization;
using System.Windows.Controls;

namespace AkmetronTask.Validation
{
    public class AmplitudeRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string s = ((string)value).Trim();

            if (string.IsNullOrWhiteSpace(s))
                return new ValidationResult(false, "Сторка не должна быть пустой");
            
            if (!double.TryParse(s, out double amp))
                return new ValidationResult(false, "Ошибка в числе: недопустимые символы или слишком большое для типа Double число");
            

            if (amp < 0 || amp > 300)
                return new ValidationResult(false, "Ошибка в числе: допускаются только значения от 0 до 300 (включительно)");

            return new ValidationResult(true, null);
        }
    }
}
