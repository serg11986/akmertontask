﻿using System.Globalization;
using System.Windows.Controls;

namespace AkmetronTask.Validation
{
    public class SecondsRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int s = 0;

            if (string.IsNullOrWhiteSpace((string)value))
                return new ValidationResult(false, "Сторка не должна быть пустой");

            try
            {
                s = int.Parse((string)value);
            }
            catch
            {
                return new ValidationResult(false, "Недопустимые символы или слишком большое число");
            }

            if ((s < 0) || (s > 59))
            {
                return new ValidationResult(false,
                  "Количество секунд должно быть от 0 до 59 (включительно)");
            }
            else
            {
                return new ValidationResult(true, null);
            }
        }
    }
}
