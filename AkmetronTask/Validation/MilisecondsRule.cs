﻿using System.Globalization;
using System.Windows.Controls;

namespace AkmetronTask.Validation
{
    public class MilisecondsRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int ms = 0;

            if (string.IsNullOrWhiteSpace((string)value))
                return new ValidationResult(false, "Сторка не должна быть пустой");

            try
            {
                ms = int.Parse((string)value);
            }
            catch
            {
                return new ValidationResult(false, "Недопустимые символы или слишком большое число");
            }

            if ((ms < 0) || (ms > 999))
            {
                return new ValidationResult(false,
                  "Количество милисекунд должно быть от 0 до 999 (включительно)");
            }
            else
            {
                return new ValidationResult(true, null);
            }
        }
    }
}
