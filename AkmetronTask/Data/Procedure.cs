﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Timers;
using System.Linq;
using System.IO;

namespace AkmetronTask.Data
{
    public enum ProcedureState { STOPPED, WORKING, PAUSED }

    public interface IProcedureElement<out T> : INotifyPropertyChanged
    {
        string Name { get; set; }
        T Data { get; }
        TimeSpan Duration { get; set; }
        bool IsFree { get; set; }
        event EventHandler Finished;
        void Start();
        void Pause();
        void Stop();
    }

    public class ProcedureElement<T> : INotifyPropertyChanged, IProcedureElement<T> 
    {

        #region Private Fields

        private string name;

        private TimeSpan duration;

        private TimeSpan timeElapsed;

        private readonly Timer timer = new Timer(Procedure<T>.TimerInterval);

        private bool isFree = true;

        private int seconds;

        private int milliSeconds;

        private double tickWeightInProgress;

        private double progress;

        #endregion

        #region Ctors
        
        public ProcedureElement(T data) : this(data, "<не задано>") { }
        
        public ProcedureElement(T data, string name)
        {
            Data = data;
            Name = name;
            Duration = TimeSpan.FromMilliseconds(Procedure<T>.TimerInterval * 5);
            timer.Elapsed += Timer_Elapsed;
        }


        #endregion

        #region EventHandlers

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timeElapsed += TimeSpan.FromMilliseconds(Procedure<T>.TimerInterval);
            Progress += tickWeightInProgress;
            if (timeElapsed >= duration)
                Stop();
        }

        #endregion

        #region Public Props

        public string Name
        {
            get => name;
            set
            {
                name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Name)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ToString"));
            }
        }

        public bool IsFree
        {
            get => isFree;
            set
            {
                isFree = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsFree)));
            }
        }

        public T Data { get; set; }

        public double Progress
        {
            get => progress;
            set
            {
                progress = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Progress)));
            }
        }

        public TimeSpan Duration
        {
            get => duration;
            set
            {
                duration = value;
                seconds = (int)duration.TotalSeconds;
                milliSeconds = (int)duration.TotalMilliseconds % 1000;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Duration)));
            }
        }

        public int Seconds
        {
            get => seconds;
            set
            {
                seconds = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Seconds)));
                duration = TimeSpan.FromSeconds(seconds).Add(TimeSpan.FromMilliseconds(milliSeconds));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Duration)));
            }
        }

        public int MilliSeconds
        {
            get => milliSeconds;
            set
            {
                milliSeconds = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MilliSeconds)));
                duration = TimeSpan.FromSeconds(seconds).Add(TimeSpan.FromMilliseconds(milliSeconds));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Duration)));
            }
        }

        #endregion

        #region Public Methods

        public void Start()
        {
            if (timeElapsed.TotalMilliseconds == 0)
            {
                tickWeightInProgress = Procedure<T>.TimerInterval / duration.TotalMilliseconds * 100;
                Progress = 0;
            }
            timer.Start();
        }

        public void Pause() => timer.Stop();

        public void Stop()
        {
            timeElapsed = TimeSpan.FromMilliseconds(0);
            timer.Stop();
            Finished?.Invoke(this, EventArgs.Empty);
        }



        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler Finished;

        #endregion

    }

    public class Procedure<T> : INotifyPropertyChanged 
    {      

        #region Constants

        public const int TimerInterval = 1;

        #endregion

        #region Private Fields
        

        private TimeSpan timeElapsed = new TimeSpan();

        private Timer timer = new Timer(TimerInterval);

        private int curElementIndex;

        private ProcedureState state = ProcedureState.STOPPED;

        private IProcedureElement<T> currentElement;
        
        private double tickWeightInProgress;

        private double progress;

        #endregion

        #region Ctors

        public Procedure()
        {
            timer.Elapsed += Timer_Elapsed;
        }


        #endregion

        #region EventHandlers

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TimeElapsed = timeElapsed.Add(TimeSpan.FromMilliseconds(TimerInterval));
            if (curElementIndex < Elements.Count && timeElapsed > TimeSpan.FromMilliseconds(1))
                Progress += tickWeightInProgress;
        }

        private void Element_Finished(object sender, EventArgs e)
        {

            if (State == ProcedureState.STOPPED)
                return;
            curElementIndex++;
            Console.WriteLine(curElementIndex);
            if (curElementIndex >= Elements.Count)
            {
                Progress = 100;
                Stop();
                return;
            }
            CurrentElement = Elements[curElementIndex];
            if (State == ProcedureState.WORKING)
                CurrentElement.Start();
        }

        #endregion

        #region Public Props


        public ProcedureState State
        {
            get => state;
            private set
            {
                state = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State)));
            }
        }

        public IProcedureElement<T> CurrentElement
        {
            get => currentElement;
            private set
            {
                currentElement = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentElement)));
            }
        }

        public ObservableCollection<IProcedureElement<T>> Elements { get; } = new ObservableCollection<IProcedureElement<T>>();

        public TimeSpan TimeElapsed
        {
            get => timeElapsed;
            private set
            {
                timeElapsed = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TimeElapsed)));
            }
        }

        public double Progress
        {
            get => progress;
            set
            {
                progress = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Progress)));
            }
        }

        
        #endregion

        #region Public Methods

        public void BindAllElements()
        {
            foreach (var el in Elements)
            {
                el.Finished += Element_Finished;
                el.IsFree = false;
            }
        }

        public void Add(IProcedureElement<T> element)
        {
            Elements.Add(element);
            element.IsFree = false;
            element.Finished += Element_Finished;
        }

        public void Remove(IProcedureElement<T> element)
        {
            Elements.Remove(element);
            element.IsFree = true;
            element.Finished -= Element_Finished;
        }


        public void Start()
        {
            if (curElementIndex == 0)
            {
                var totalMilliseconds = Elements.Sum(e => e.Duration.TotalMilliseconds);
                tickWeightInProgress = TimerInterval / totalMilliseconds * 100;
                Progress = 0;
            }
            CurrentElement = Elements[curElementIndex];
            State = ProcedureState.WORKING;
            timer.Start();
            CurrentElement.Start();
        }

        public void Pause()
        {
            State = ProcedureState.PAUSED;
            timer.Stop();
            CurrentElement.Pause();
        }

        public void Stop()
        {
            State = ProcedureState.STOPPED;
            timer.Stop();
            CurrentElement.Stop();
            timeElapsed = new TimeSpan();
            curElementIndex = 0;
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

    }

}
