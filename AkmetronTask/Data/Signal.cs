﻿namespace AkmetronTask.Data
{
    public abstract class Signal
    {
        public virtual double Frequency { get; set; } = 30;

        public virtual double Amplitude { get; set; } = 50;
    }

    public class SinusoidalSignal : Signal
    {
        public double PhaseShift { get; set; } = 20;
    }

    public class FMSignal : Signal
    {
        private readonly Signal carrierSignal;

        private Signal informationSignal;

        private Signal modulatedSignal;

        //заглушка для поддержания работы
        public FMSignal() : this(new SinusoidalSignal(), new SinusoidalSignal()) { }

        public FMSignal(Signal carrierSignal, Signal informationSignal)
        {
            this.carrierSignal = carrierSignal;
            this.informationSignal = informationSignal;
            modulatedSignal = Modulate(carrierSignal, informationSignal);
        }

        private Signal Modulate(Signal carrierSignal, Signal informationSignal)
        {
            //заглушка для поддержания работы
            return informationSignal;
        }

        private Signal Demodulate(Signal carrierSignal, Signal modulatedSignal)
        {
            //заглушка для поддержания работы
            return modulatedSignal;
        }

        public double CarrierSignalFrequency
        {
            get => carrierSignal.Frequency;
            set
            {
                carrierSignal.Frequency = value;
                modulatedSignal = Modulate(carrierSignal, informationSignal);
            }
        } 

        public double CarrierSignalAmplitude
        {
            get => carrierSignal.Amplitude;
            set
            {
                carrierSignal.Amplitude = value;
                modulatedSignal.Amplitude = value;
            }
        } 

        public override double Frequency
        {
            get => modulatedSignal.Frequency;
            set
            {
                modulatedSignal.Frequency = value;
                informationSignal = Demodulate(carrierSignal, modulatedSignal);
            }
        }

        public override double Amplitude
        {
            get => modulatedSignal.Amplitude;
            set
            {
                modulatedSignal.Amplitude = value;
                informationSignal.Amplitude = value;
            }
        }
    }
}
