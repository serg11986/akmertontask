﻿using AkmetronTask.IO;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AkmetronTask.Data
{
    public class Model : INotifyPropertyChanged
    {

        #region Private Fields

        private readonly ILoader<Procedure<Signal>> loader;

        private readonly ISaver<Procedure<Signal>> saver;
        
        private Procedure<Signal> procedure = new Procedure<Signal>();

        private int signalsCounter = 1;

        private bool isFree = true;

        private TimeSpan timeElapsed;

        private double progress;
        
        private IProcedureElement<SinusoidalSignal> currentSinusoidalSignal = new ProcedureElement<SinusoidalSignal>(new SinusoidalSignal(), "сигнал 1");

        private IProcedureElement<FMSignal> currentFMSignal = new ProcedureElement<FMSignal>(new FMSignal(), "сигнал 1");

        private IProcedureElement<Signal> currentSignal;

        private bool isCurrentSignalSinusoidal;

        private bool isCurrentSignalFM;

        private ProcedureState state = ProcedureState.STOPPED;

        #endregion

        #region Ctors

        public Model(ILoader<Procedure<Signal>> loader, ISaver<Procedure<Signal>> saver)
        {
            this.loader = loader;
            this.saver = saver;
            CurrentSignal = CurrentFMSignal;
            procedure.PropertyChanged += Procedure_PropertyChanged;
        }
        

        #endregion

        #region EventHandlers

        private void Procedure_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(procedure.CurrentElement):
                    CurrentSignal = procedure.CurrentElement;
                    break;
                case nameof(procedure.State):
                    State = procedure.State;
                    IsFree = procedure.State == ProcedureState.STOPPED;
                    if (IsFree)
                        FinishedPlaying?.Invoke(this, EventArgs.Empty);
                    break;
                case nameof(procedure.TimeElapsed):
                    TimeElapsed = procedure.TimeElapsed;
                    break;
                case nameof(procedure.Progress):
                    Progress = procedure.Progress;
                    break;
            }
        }

        #endregion

        #region Public Props
        
        public ReadOnlyObservableCollection<IProcedureElement<Signal>> Elements => new ReadOnlyObservableCollection<IProcedureElement<Signal>>(procedure.Elements);

        public TimeSpan TimeElapsed
        {
            get => timeElapsed;
            set
            {
                timeElapsed = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TimeElapsed)));
            }
        }

        public double Progress
        {
            get => progress;
            set
            {
                progress = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Progress)));
            }
        }

        public bool IsCurrentSignalSinusoidal
        {
            get => isCurrentSignalSinusoidal;
            set
            {
                isCurrentSignalSinusoidal = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsCurrentSignalSinusoidal)));
                if (isCurrentSignalSinusoidal && currentSignal == currentFMSignal)
                    CurrentSignal = CurrentSinusoidalSignal;
            }
        }

        public bool IsCurrentSignalFM
        {
            get => isCurrentSignalFM;
            set
            {
                isCurrentSignalFM = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsCurrentSignalFM)));
                if (isCurrentSignalFM && currentSignal == currentSinusoidalSignal)
                    CurrentSignal = CurrentFMSignal;
            }
        }

        
        public IProcedureElement<SinusoidalSignal> CurrentSinusoidalSignal
        {
            get => currentSinusoidalSignal;
            private set
            {
                currentSinusoidalSignal = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentSinusoidalSignal)));
            }
        }

        public IProcedureElement<FMSignal> CurrentFMSignal
        {
            get => currentFMSignal;
            private set
            {
                currentFMSignal = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentFMSignal)));
            }
        }

        public IProcedureElement<Signal> CurrentSignal
        {
            get => currentSignal;
            set
            {
                if (value == null)
                    UpdateCurrentSignal();
                else
                    currentSignal = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentSignal)));
                IsCurrentSignalSinusoidal = currentSignal.Data is SinusoidalSignal;
                IsCurrentSignalFM = currentSignal.Data is FMSignal;
            }
        }

        public bool IsFree
        {
            get => isFree;
            set
            {
                isFree = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsFree)));
            }
        }

        public ProcedureState State
        {
            get => state;
            set
            {
                state = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State)));
            }
        }

        #endregion

        #region Public Methods

        public void UpdateCurrentSignal()
        {
            if (IsCurrentSignalSinusoidal)
                CurrentSignal = CurrentSinusoidalSignal;
            if (IsCurrentSignalFM)
                CurrentSignal = CurrentFMSignal;
        }

        public void AddCurrentSignalToProcedure()
        {
            procedure.Add(CurrentSignal);
            signalsCounter++;
            CurrentSinusoidalSignal = new ProcedureElement<SinusoidalSignal>(new SinusoidalSignal(), $"сигнал {signalsCounter}");
            CurrentFMSignal = new ProcedureElement<FMSignal>(new FMSignal(), $"сигнал {signalsCounter}");
            UpdateCurrentSignal();
        }

        public void RemoveCurrentSignalFromProcedure()
        {
            procedure.Remove(CurrentSignal);
        }

        public void Save()
        {
            saver.Save(procedure);
        }
        
        public void Open()
        {
            var procedure = loader.Load();
            if (procedure == null)
                return;
            this.procedure = procedure;
            Progress = 0;
            TimeElapsed = new TimeSpan();
            procedure.PropertyChanged += Procedure_PropertyChanged;
            procedure.BindAllElements();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Elements)));
        }

        public void New()
        {
            procedure = new Procedure<Signal>();
            Progress = 0;
            TimeElapsed = new TimeSpan();
            procedure.PropertyChanged += Procedure_PropertyChanged;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Elements)));
        }
        

        public void Start() => procedure.Start();
        public void Pause() => procedure.Pause();
        public void Stop() => procedure.Stop();

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler FinishedPlaying;

        #endregion

    }
}
