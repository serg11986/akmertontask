﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkmetronTask.Data
{
    public enum FrequencyMeasure { G, MG, GG }

    public struct Frequency
    {
        public double Value { get; set; }
        public FrequencyMeasure Measure { get; set; }
    }
}
