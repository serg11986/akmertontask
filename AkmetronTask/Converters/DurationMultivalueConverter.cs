﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AkmetronTask.Converters
{
    public class DurationMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (double.TryParse((string)values[0], out double first))
            {
                if (first < 0)
                    first = 0;
                if (first > 59)
                    first = 59;
                if (double.TryParse((string)values[1], out double second))
                {
                    if (second < 0)
                        second = 0;
                    if (second > 999)
                        second = 999;
                    return first + second / 1000;
                }
                else
                    return first;
            }
            return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { Math.Floor((double)value) + "", (int)(((double)value - Math.Floor((double)value)) * 1000) + "" };
        }
    }
}
