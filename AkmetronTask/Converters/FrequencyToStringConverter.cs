﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace AkmetronTask.Converters
{
    class FrequencyToStringConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double freq = (double)value;

            string[] measures = new string[] { "Гц", "КГц", "МГц", "ГГц" };

            int i = 0;

            while (freq > 500 && i < 4)
            {
                freq /= 1000;
                i++;
            }

            return Math.Round(freq, 2) + " " + measures[i];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] split = ((string)value).Split().Select(w => w.Trim().ToLower()).Where(w => !string.IsNullOrEmpty(w)).ToArray();

            string[] measures = new string[] { "гц", "кгц", "мгц", "ггц" };

            double freq = double.Parse(split[0]);

            int pow = Array.IndexOf(measures, split[1].Trim());

            freq *= Math.Pow(1000, pow);

            return freq;

        }
    }
}
