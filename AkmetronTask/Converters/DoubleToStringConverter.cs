﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AkmetronTask.Converters
{
    class DoubleToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            Math.Round((double)value, 2) + "";

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            double.Parse((string)value);
    }
}
