﻿using AkmetronTask.Data;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.IO;

namespace AkmetronTask.IO
{

    public interface ILoader<out T>
    {
        T Load();
    }

    public interface ISaver<in T>
    {
        bool Save(T data);
    }

   public class JSONFileLoaderSaverWithTypeHandlingAll<T> : ILoader<T>, ISaver<T>
    {
        public T Load()
        {
            try
            {
                string fileName;
                OpenFileDialog myDialog = new OpenFileDialog();
                myDialog.Filter = "JSON|*.json";
                myDialog.CheckFileExists = true;
                if (myDialog.ShowDialog() == true)
                {
                    fileName = myDialog.FileName;
                }
                else
                    return default(T);
                var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
                string json = File.ReadAllText(fileName);
                var data = JsonConvert.DeserializeObject<T>(json, settings);
                return data;
            }
            catch
            {
                return default(T);
            }
        }

        public bool Save(T data)
        {
            try
            {
                string fileName;
                SaveFileDialog myDialog = new SaveFileDialog();
                myDialog.Filter = "JSON|*.json";
                if (myDialog.ShowDialog() == true)
                {
                    fileName = myDialog.FileName;
                }
                else
                    return false;
                var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
                string json = JsonConvert.SerializeObject(data, Formatting.Indented, settings);
                File.WriteAllText(fileName, json);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    class JSONFileLoaderSaverForProcedures<T> : JSONFileLoaderSaverWithTypeHandlingAll<Procedure<T>> 
    {
    }

    class JSONFileLoaderSaverForProceduresOfSignals : JSONFileLoaderSaverForProcedures<Signal>
    {

    }


}
