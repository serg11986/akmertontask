﻿using AkmetronTask.Data;
using System.Windows;
using System.Windows.Controls;

namespace AkmetronTask.Selectors
{
    public class SignalTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SinusoidalSignalTemplate { get; set; }

        public DataTemplate FMSignalTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var procedureElement = (Signal)item;

            return procedureElement is SinusoidalSignal ? SinusoidalSignalTemplate : procedureElement is FMSignal ? FMSignalTemplate : null; 
        }
    }

}
